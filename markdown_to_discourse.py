#! /usr/bin/env python3


# service-public-to-markdown -- Convert Service-Public.fr XML pages to Markdown
# By: Paula Forteza <paula@forteza.fr>
#     Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
# https://git.framasoft.org/francais-du-monde/service-public-to-markdown
#
# service-public-to-markdown is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# service-public-to-markdown is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Update Discourse forum with Markdown pages generated from service-public.fr pages."""


import argparse
import itertools
import json
import logging
import os
import re
import shutil
import sys
import time
import urllib.error
import urllib.parse
import urllib.request


app_name = os.path.splitext(os.path.basename(__file__))[0]
args = None
bot_tag = 'service-public'
category_id = 5  # Droits et démarches
footer_link_re = re.compile('Cette fiche est issue de <a href="https://www\.service-public\.fr/'
    '(?P<part>associations|particuliers|professionnels-entreprises)/vosdroits/(?P<id>[FNR]\d+)"')
log = logging.getLogger(app_name)
markdown_re = re.compile('(?ims)# (?P<title>.*?)\n\n(?P<body>.*)$')


def delete(path):
    query = urllib.parse.urlencode(dict(
        api_key = args.api_key,
        api_username = args.username,
        ))
    request = urllib.request.Request(
        method = 'DELETE',
        url = urllib.parse.urljoin(args.forum_url, '{}?{}'.format(path, query)),
        )
    for retry in range(5):
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            if e.code == 429:  # Too Many Requests
                print("Sleeping because too many requests...")
                time.sleep(10)
            elif e.code == 500 and retry == 0:  # Internal Server Error (retry only once)
                print("Sleeping because of internal servor error...")
                time.sleep(10)
            elif e.code == 502:  # Bad Gateway
                print("Sleeping because of bad gateway...")
                time.sleep(10)
            else:
                print('Error response {}:\n{}'.format(e.code, e.read().decode('utf-8')))
                raise
        else:
            # return json.loads(response.read().decode('utf-8'))
            return None
    else:
        raise Exception("Too many errors.")


def get(path):
    query = urllib.parse.urlencode(dict(
        api_key = args.api_key,
        api_username = args.username,
        ))
    request = urllib.request.Request(
        url = urllib.parse.urljoin(args.forum_url, '{}?{}'.format(path, query)),
        )
    for retry in range(5):
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            if e.code == 429:  # Too Many Requests
                print("Sleeping because too many requests...")
                time.sleep(10)
            elif e.code == 500 and retry == 0:  # Internal Server Error (retry only once)
                print("Sleeping because of internal servor error...")
                time.sleep(10)
            elif e.code == 502:  # Bad Gateway
                print("Sleeping because of bad gateway...")
                time.sleep(10)
            else:
                print('Error response {}:\n{}'.format(e.code, e.read().decode('utf-8')))
                raise
        else:
            return json.loads(response.read().decode('utf-8'))
    else:
        raise Exception("Too many errors.")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('xml_dir', help = 'path of directory containing XML files')
    parser.add_argument('markdown_dir', help = 'path of directory containing Markdown files')
    parser.add_argument('forum_url', help = 'URL of Discourse forum')
    parser.add_argument('-k', '--api-key', required = True, help = 'Discourse API key')
    parser.add_argument('-u', '--username', required = True, help = 'Discourse username')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = 'increase output verbosity')
    global args
    args = parser.parse_args()

    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    assert os.path.exists(args.markdown_dir)
    assert os.path.exists(args.xml_dir)

    # Use XML files to associate each card ID with the parts it belongs to.
    parts_by_card_id = {}
    for part in (
            'associations',
            'particuliers',
            'professionnels',
            ):
        xml_dir = os.path.join(args.xml_dir, part)
        assert os.path.exists(xml_dir)
        for filename in os.listdir(xml_dir):
            if filename.startswith('.'):
                continue
            card_id, extension = os.path.splitext(filename)
            if extension != '.xml':
                continue
            parts_by_card_id.setdefault(card_id, []).append(part)

    # Retrieve existing pages in Discourse forum.
    log.info('Retrieving existing topics...')
    existing_topics = []
    topic_by_card_url = {}
    try:
        for page in itertools.count(0):
            tag_data = get('/tags/{}.json?page={}'.format(bot_tag, page))
            for topic_digest in tag_data['topic_list']['topics']:
                topic = get('/t/{}.json'.format(topic_digest['id']))
                posts = topic['post_stream']['posts']
                main_post_digest = posts[0]
                match = footer_link_re.search(main_post_digest['cooked'])
                if match is None:
                    log.warning('Ignoring existing topic {} "{}" not related to a service-public.fr card'.format(
                        topic['id'], topic['title']))
                    continue
                existing_topics.append(topic)
                topic_by_card_id[match.group('id')] = topic
            if tag_data['topic_list'].get('more_topics_url') is None:
                break
    except urllib.error.HTTPError as e:
        # A 404 occurs when tag doesn't exist yet.'
        if e.code != 404:
            raise
    log.info('  {} existing topics found.'.format(len(existing_topics)))

    current_topics_id = set()
    markdown_body_by_card_id = {}
    for part in (
            'associations',
            'particuliers',
            'professionnels',
            ):
        markdown_dir = os.path.join(args.markdown_dir, part)
        assert os.path.exists(markdown_dir)
        for filename in os.listdir(markdown_dir):
            if filename.startswith('.'):
                continue
            card_id, extension = os.path.splitext(filename)
            if extension != '.md':
                continue
            markdown_file_path = os.path.join(markdown_dir, filename)
            with open(markdown_file_path) as markdown_file:
                markdown = markdown_file.read()
                match = markdown_re.match(markdown)
                if match is None:
                    log.warning('Ignoring file {} with unexpected markdown'.format(markdown_file_path))
                    continue
                body = match.group('body')
                # Cleanup body to ensure that Discourse use the same one.
                body = body.replace(' ', ' ')  # Replace no break space with space.
                markdown_body_by_card_id[card_id] = body
                title = match.group('title')
                # Cleanup title to ensure that Discourse use the same one.
                title = title.replace(' ', ' ')  # Replace no break space with space.
                while '  ' in title:
                    title = title.replace('  ', ' ')
                if title.isupper() or title.islower():
                    title = title.capitalize()
                tags = [bot_tag]
                for card_part in parts_by_card_id[card_id]:
                    if card_part == 'associations':
                        tags.append('association')
                    elif card_part == 'particuliers':
                        tags.append('particulier')
                    elif card_part == 'professionnels':
                        tags.extend(['entreprise', 'professionnel'])
                topic = topic_by_card_id.get(card_id)
                if topic is None:
                    log.info('Creating topic {} {}'.format(card_id, title))
                    parameters = [
                        ('category', category_id),
                        ('raw', body),
                        ('title', title),
                        ]
                    for tag in tags:
                        parameters.append(('tags[]', tag))
                    change = post('/posts', parameters)
                    topic = get('/t/{}.json'.format(change['topic_id']))
                    topic_by_card_id[card_id] = topic
                elif topic['category_id'] != category_id or topic['title'] != title \
                        or not set(topic['tags']).issuperset(tags):
                    log.info('Updating topic {} {}'.format(card_id, title))
                    if topic['category_id'] != category_id:
                        log.info('  Updating category {} {}'.format(topic['category_id'], category_id))
                    if topic['title'] != title:
                        log.info('  Updating title "{}" "{}"'.format(topic['title'], title))
                    if not set(topic['tags']).issuperset(tags):
                        log.info('  Updating category {} {}'.format(topic['tags'], tags))
                    parameters = [
                        ('category_id', category_id),
                        ('title', title),
                        ('topic_id', topic['id']),
                        ]
                    for tag in tags:
                        parameters.append(('tags[]', tag))
                    change = put('/t/{}'.format(topic['id']), parameters)
                    topic = get('/t/{}.json'.format(topic['id']))
                    topic_by_card_id[card_id] = topic
                current_topics_id.add(topic['id'])

    # Now that every card has its own topic, replace the internal URLs in markdown cards with those of forum.
    for card_id, body in markdown_body_by_card_id.items():
        # Replace URLs in body.
        card_parts = parts_by_card_id[card_id]
        for link_id, link_parts in parts_by_card_id.items():
            linked_topic = topic_by_card_id.get(link_id)
            if linked_topic is None:
                # Choose the best URL to link from card to service-public.fr site
                for possible_part in ('particuliers', 'professionnels', 'associations'):
                    if possible_part in link_parts and possible_part in card_parts:
                        link_part = possible_part
                        break
                else:
                    for possible_part in ('particuliers', 'professionnels', 'associations'):
                        if possible_part in link_parts:
                            link_part = possible_part
                            break
                    else:
                        raise KeyError('Card {} belongs to unknown parts: {}'.format(link_id, link_parts))
                linked_url = 'https://www.service-public.fr/{}/vosdroits/{}'.format(link_part, link_id)
            else:
                linked_url = '/t/{}/{}'.format(linked_topic['slug'], linked_topic['id'])
            body = body.replace('{}.md'.format(link_id), linked_url)

        # Update topic main post if needed.
        topic = topic_by_card_id[card_id]
        posts = topic['post_stream']['posts']
        main_post_digest = posts[0]
        main_post = get('/posts/{}.json'.format(main_post_digest['id']))
        if main_post['raw'].strip() != body.strip():
            log.info('Updating topic main post {} {}'.format(card_id, topic['title']))
            put('/posts/{}'.format(main_post['id']), json.dumps(dict(raw = body)),
                headers = {
                    'Content-Type': 'application/json',
                    },
                )

    # Delete obsolete topics.
    for topic in existing_topics:
        if topic['id'] not in current_topics_id:
            log.info('Deleting topic {} {}'.format(topic['id'], topic['title']))
            delete('/t/{}.json'.format(topic['id']))

    return 0


def post(path, parameters, headers = {}):
    query = urllib.parse.urlencode(dict(
        api_key = args.api_key,
        api_username = args.username,
        ))
    body = parameters.encode('utf-8') if isinstance(parameters, str) \
        else urllib.parse.urlencode(parameters).encode('utf-8')
    request = urllib.request.Request(
        data = body,
        headers = headers,
        url = urllib.parse.urljoin(args.forum_url, '{}?{}'.format(path, query)),
        )
    for retry in range(5):
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            if e.code == 429:  # Too Many Requests
                print("Sleeping because too many requests...")
                time.sleep(10)
            elif e.code == 500 and retry == 0:  # Internal Server Error (retry only once)
                print("Sleeping because of internal servor error...")
                time.sleep(10)
            elif e.code == 502:  # Bad Gateway
                print("Sleeping because of bad gateway...")
                time.sleep(10)
            else:
                print('Error response {}:\n{}'.format(e.code, e.read().decode('utf-8')))
                raise
        else:
            return json.loads(response.read().decode('utf-8'))
    else:
        raise Exception("Too many errors.")


def put(path, parameters, headers = {}):
    query = urllib.parse.urlencode(dict(
        api_key = args.api_key,
        api_username = args.username,
        ))
    body = parameters.encode('utf-8') if isinstance(parameters, str) \
        else urllib.parse.urlencode(parameters).encode('utf-8')
    request = urllib.request.Request(
        data = body,
        headers = headers,
        method = 'PUT',
        url = urllib.parse.urljoin(args.forum_url, '{}?{}'.format(path, query)),
        )
    for retry in range(5):
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            if e.code == 429:  # Too Many Requests
                print("Sleeping because too many requests...")
                time.sleep(10)
            elif e.code == 500 and retry == 0:  # Internal Server Error (retry only once)
                print("Sleeping because of internal servor error...")
                time.sleep(10)
            elif e.code == 502:  # Bad Gateway
                print("Sleeping because of bad gateway...")
                time.sleep(10)
            else:
                print('Error response {}:\n{}'.format(e.code, e.read().decode('utf-8')))
                raise
        else:
            return json.loads(response.read().decode('utf-8'))
    else:
        raise Exception("Too many errors.")


if __name__ == '__main__':
    sys.exit(main())
