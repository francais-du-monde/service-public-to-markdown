# service-public-to-markdown

Convert [Service-Public.fr](https://www.service-public.fr/) XML pages to Markdown and include them in the
[Discourse-based forum of Français du monde](https://forum.francais-du-monde.fr/).

* [About XML pages availability as open data](https://www.service-public.fr/a-propos/donnees-ouvertes)
* [XML pages download & documentation](https://www.service-public.fr/partenaires/comarquage/documentation)
* [More documentation](https://www.service-public.fr/partenaires/comarquage/mise-oeuvre)

## Usage

```bash
./harvest_xml.py -c -p ../service-public-vos-droits-xml/
./xml_to_markdown.py -c -p ../service-public-vos-droits-xml/ ../service-public-vos-droits-markdown/
./markdown_to_discourse.py -k DISCOURSE_API_KEY -u DISCOURSE_API_USERNAME -v ../service-public-vos-droits-xml/ ../service-public-vos-droits-markdown/ https://forum.francais-du-monde.fr/
```
