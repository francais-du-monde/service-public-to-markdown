> service-public-to-markdown -- Convert Service-Public.fr XML pages to Markdown
> By: Paula Forteza <paula@forteza.fr>
>     Emmanuel Raviart <emmanuel@raviart.com>
>
> Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
> https://git.framasoft.org/francais-du-monde/service-public-to-markdown
>
> service-public-to-markdown is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> service-public-to-markdown is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <http:>www.gnu.org/licenses/>.
