#! /usr/bin/env python3


# service-public-to-markdown -- Convert Service-Public.fr XML pages to Markdown
# By: Paula Forteza <paula@forteza.fr>
#     Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
# https://git.framasoft.org/francais-du-monde/service-public-to-markdown
#
# service-public-to-markdown is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# service-public-to-markdown is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Harvest XML pages from Service-Public.fr."""


import argparse
import io
import os
import shutil
import subprocess
import sys
import urllib.request
import zipfile


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('target_dir', help = 'path of target directory for harvested XML pages')
    parser.add_argument('-c', '--commit', action = 'store_true', default = False, help = 'commit XML pages')
    parser.add_argument('-p', '--push', action = 'store_true', default = False, help = 'push commit')
    args = parser.parse_args()

    if not os.path.exists(args.target_dir):
        os.makedirs(args.target_dir)

    for part, archive_url in (
            ('associations', 'http://lecomarquage.service-public.fr/vdd/3.0/asso/zip/vosdroits-latest.zip'),
            ('particuliers', 'https://lecomarquage.service-public.fr/vdd/3.0/part/zip/vosdroits-latest.zip'),
            ('professionnels', 'https://lecomarquage.service-public.fr/vdd/3.0/pro/zip/vosdroits-latest.zip'),
            ):
        response = urllib.request.urlopen(archive_url)
        with zipfile.ZipFile(io.BytesIO(response.read())) as archive_zip:
            part_dir = os.path.join(args.target_dir, part)

            # Cleanup target directory.
            if os.path.exists(part_dir):
                shutil.rmtree(part_dir)
            os.makedirs(part_dir)

            # Extract XML files
            archive_zip.extractall(part_dir)

    if args.commit:
        subprocess.check_call(['git', 'add', '.'], cwd = args.target_dir)
        process = subprocess.Popen(['git', 'commit', '-a', '-m Nouvelle récolte'], cwd = args.target_dir,
            stderr = subprocess.PIPE, stdout = subprocess.PIPE)
        output, error = process.communicate()
        if process.returncode == 0:
            if args.push:
                subprocess.check_call(['git', 'push', 'origin', 'master'], cwd = args.target_dir)
        else:
            assert process.returncode == 1 and b"nothing to commit" in output, \
                'An error {} occurred while committing:\noutput:\n{}\nerror:\n{}'.format(
                    process.returncode, output, error)

    return 0


if __name__ == '__main__':
    sys.exit(main())
